#![allow(unused_imports, unused_variables)] // Allowed for now since we are prototyping

use gerst::{
    camera::{self, Camera},
    renderer::{
        buffer::VertexBuffer,
        shader::{Shader, ShaderProgramBuilder, ShaderType, UniformValue},
        texture::Texture,
        vertex_array::{VertexArray, VertexArrayBuilder},
        RenderingContext,
    },
    Image, Vertex, V2, V3,
};

use glfw::{Action, Context, Key};
use log::{debug, error, info, trace};
use nalgebra::{Isometry3, Perspective3, Translation3, Unit, UnitQuaternion, Vector3};
use std::error::Error;
use std::ffi::{c_void, CStr};
use std::iter;
use std::rc::Rc;

const OBJECT_VERTEX_SHADER_SOURCE: &str = include_str!("shaders/object.vert");
const OBJECT_FRAGMENT_SHADER_SOURCE: &str = include_str!("shaders/object.frag");
const LIGHTNING_VERTEX_SHADER_SOURCE: &str = include_str!("shaders/light_object.vert");
const LIGHTNING_FRAGMENT_SHADER_SOURCE: &str = include_str!("shaders/light_object.frag");

extern "system" fn debug_callback(
    source: u32,
    error_type: u32,
    id: u32,
    severity: u32,
    _length: i32,
    message_ptr: *const i8,
    _userptr: *mut c_void,
) {
    let message = unsafe {
        let str = CStr::from_ptr(message_ptr);
        str.to_str().unwrap()
    };
    let source = match source {
        gl::DEBUG_SOURCE_API => "API",
        gl::DEBUG_SOURCE_WINDOW_SYSTEM => "Window System",
        gl::DEBUG_SOURCE_SHADER_COMPILER => "Shader Compiler",
        gl::DEBUG_SOURCE_THIRD_PARTY => "Third Party",
        gl::DEBUG_SOURCE_APPLICATION => "Application",
        gl::DEBUG_SOURCE_OTHER => "Other",
        _ => "Unknown source",
    };
    let error_type = match error_type {
        gl::DEBUG_TYPE_ERROR => "Error",
        gl::DEBUG_TYPE_DEPRECATED_BEHAVIOR => "Deprecated Behaviour",
        gl::DEBUG_TYPE_UNDEFINED_BEHAVIOR => "Undefined Behaviour",
        gl::DEBUG_TYPE_PORTABILITY => "Portability",
        gl::DEBUG_TYPE_PERFORMANCE => "Performance",
        gl::DEBUG_TYPE_MARKER => "Marker",
        gl::DEBUG_TYPE_PUSH_GROUP => "Push Group",
        gl::DEBUG_TYPE_POP_GROUP => "Pop Group",
        gl::DEBUG_TYPE_OTHER => "Other",
        _ => "Unknown type",
    };
    let severity = match severity {
        gl::DEBUG_SEVERITY_HIGH => "High",
        gl::DEBUG_SEVERITY_MEDIUM => "Medium",
        gl::DEBUG_SEVERITY_LOW => "Low",
        gl::DEBUG_SEVERITY_NOTIFICATION => "Notification",
        _ => "Unknown severity",
    };

    if severity == "High" {
        error!("-----");
        error!("Debug message ({}): {}", id, message);
        error!("Source: {}", source);
        error!("Type: {}", error_type);
        error!("Severity: {}", severity);
        std::process::exit(1);
    } else if severity == "Notification" {
        trace!("-----");
        trace!("Debug message ({}): {}", id, message);
        trace!("Source: {}", source);
        trace!("Type: {}", error_type);
        trace!("Severity: {}", severity);
    } else {
        debug!("-----");
        debug!("Debug message ({}): {}", id, message);
        debug!("Source: {}", source);
        debug!("Type: {}", error_type);
        debug!("Severity: {}", severity);
    }
}

fn clamp<T: PartialOrd>(current: T, min: T, max: T) -> T {
    debug_assert!(min <= max, "Min must be less or equal to max");
    if current < min {
        min
    } else if current > max {
        max
    } else {
        current
    }
}

fn main() -> Result<(), Box<dyn Error>> {
    pretty_env_logger::init();

    let cube = Box::new([
        Vertex::new(V3::new(-0.5, -0.5, -0.5), V2::new(0.0, 0.0)),
        Vertex::new(V3::new(0.5, -0.5, -0.5), V2::new(1.0, 0.0)),
        Vertex::new(V3::new(0.5, 0.5, -0.5), V2::new(1.0, 1.0)),
        Vertex::new(V3::new(0.5, 0.5, -0.5), V2::new(1.0, 1.0)),
        Vertex::new(V3::new(-0.5, 0.5, -0.5), V2::new(0.0, 1.0)),
        Vertex::new(V3::new(-0.5, -0.5, -0.5), V2::new(0.0, 0.0)),
        Vertex::new(V3::new(-0.5, -0.5, 0.5), V2::new(0.0, 0.0)),
        Vertex::new(V3::new(0.5, -0.5, 0.5), V2::new(1.0, 0.0)),
        Vertex::new(V3::new(0.5, 0.5, 0.5), V2::new(1.0, 1.0)),
        Vertex::new(V3::new(0.5, 0.5, 0.5), V2::new(1.0, 1.0)),
        Vertex::new(V3::new(-0.5, 0.5, 0.5), V2::new(0.0, 1.0)),
        Vertex::new(V3::new(-0.5, -0.5, 0.5), V2::new(0.0, 0.0)),
        Vertex::new(V3::new(-0.5, 0.5, 0.5), V2::new(1.0, 0.0)),
        Vertex::new(V3::new(-0.5, 0.5, -0.5), V2::new(1.0, 1.0)),
        Vertex::new(V3::new(-0.5, -0.5, -0.5), V2::new(0.0, 1.0)),
        Vertex::new(V3::new(-0.5, -0.5, -0.5), V2::new(0.0, 1.0)),
        Vertex::new(V3::new(-0.5, -0.5, 0.5), V2::new(0.0, 0.0)),
        Vertex::new(V3::new(-0.5, 0.5, 0.5), V2::new(1.0, 0.0)),
        Vertex::new(V3::new(0.5, 0.5, 0.5), V2::new(1.0, 0.0)),
        Vertex::new(V3::new(0.5, 0.5, -0.5), V2::new(1.0, 1.0)),
        Vertex::new(V3::new(0.5, -0.5, -0.5), V2::new(0.0, 1.0)),
        Vertex::new(V3::new(0.5, -0.5, -0.5), V2::new(0.0, 1.0)),
        Vertex::new(V3::new(0.5, -0.5, 0.5), V2::new(0.0, 0.0)),
        Vertex::new(V3::new(0.5, 0.5, 0.5), V2::new(1.0, 0.0)),
        Vertex::new(V3::new(-0.5, -0.5, -0.5), V2::new(0.0, 1.0)),
        Vertex::new(V3::new(0.5, -0.5, -0.5), V2::new(1.0, 1.0)),
        Vertex::new(V3::new(0.5, -0.5, 0.5), V2::new(1.0, 0.0)),
        Vertex::new(V3::new(0.5, -0.5, 0.5), V2::new(1.0, 0.0)),
        Vertex::new(V3::new(-0.5, -0.5, 0.5), V2::new(0.0, 0.0)),
        Vertex::new(V3::new(-0.5, -0.5, -0.5), V2::new(0.0, 1.0)),
        Vertex::new(V3::new(-0.5, 0.5, -0.5), V2::new(0.0, 1.0)),
        Vertex::new(V3::new(0.5, 0.5, -0.5), V2::new(1.0, 1.0)),
        Vertex::new(V3::new(0.5, 0.5, 0.5), V2::new(1.0, 0.0)),
        Vertex::new(V3::new(0.5, 0.5, 0.5), V2::new(1.0, 0.0)),
        Vertex::new(V3::new(-0.5, 0.5, 0.5), V2::new(0.0, 0.0)),
        Vertex::new(V3::new(-0.5, 0.5, -0.5), V2::new(0.0, 1.0)),
    ]);

    //let indices = Box::new([V3::new(0, 1, 3), V3::new(1, 2, 3)]);

    let lights = [
        V3::new(0.7_f32, 0.2, 2.0),
        V3::new(2.3, -3.3, -4.0),
        V3::new(-4.0, 2.0, -12.0),
        V3::new(0.0, 0.0, -3.0),
    ];
    let cubes = [
        V3::new(0.0, 0.0, 0.0),
        V3::new(2.0, 5.0, -15.0),
        V3::new(-1.5, -2.2, -2.5),
        V3::new(-3.8, -2.0, -12.3),
        V3::new(2.4, -0.4, -3.5),
        V3::new(-1.7, 3.0, -7.5),
        V3::new(1.3, -2.0, -2.5),
        V3::new(1.5, 2.0, -2.5),
        V3::new(1.5, 0.2, -1.5),
        V3::new(-1.3, 1.0, -1.5),
        V3::new(1.0, 0.0, 0.0),
        V3::new(5.0, 0.0, 0.0),
        V3::new(1.0, 0.0, 3.0),
    ];

    let mut width = 768;
    let mut height = 768;

    // setup window
    let mut glfw = glfw::init(glfw::fail_on_errors)?;
    glfw.window_hint(glfw::WindowHint::ContextVersion(4, 6));
    glfw.window_hint(glfw::WindowHint::OpenGlDebugContext(true));
    glfw.window_hint(glfw::WindowHint::OpenGlProfile(
        glfw::OpenGlProfileHint::Core,
    ));
    glfw.window_hint(glfw::WindowHint::Samples(Some(32)));

    let (mut window, events) = glfw
        .create_window(
            width as _,
            height as _,
            "Gerst Game Engine Sandbox",
            glfw::WindowMode::Windowed,
        )
        .unwrap();
    window.set_key_polling(true);
    window.set_size_polling(true);
    window.set_cursor_pos_polling(true);
    window.set_cursor_mode(glfw::CursorMode::Disabled);
    window.make_current();

    glfw.set_swap_interval(glfw::SwapInterval::Adaptive);

    // setup opengl
    let gl_ctx = gl::Gl::load_with(|symbol| window.get_proc_address(symbol));
    let rctx = Rc::new(RenderingContext::new(gl_ctx));

    let version = window.get_context_version();
    info!(
        "OpenGL version: {}.{}.{}",
        version.major, version.minor, version.patch
    );

    // setup gl debug logging
    unsafe {
        rctx.gl.Enable(gl::DEBUG_OUTPUT_SYNCHRONOUS);
        rctx.gl
            .DebugMessageCallback(Some(debug_callback), std::ptr::null() as _);
    }

    // setup depth test
    unsafe {
        rctx.gl.Enable(gl::DEPTH_TEST);
    }

    // Enable multisampling
    unsafe {
        rctx.gl.Enable(gl::MULTISAMPLE);
    }

    let vertex_buffer = Rc::new(VertexBuffer::new(rctx.clone(), cube).unwrap());
    //let index_buffer = Rc::new(IndexBuffer::new(rctx.clone(), indices).unwrap());

    let vertex_array = VertexArrayBuilder::new()
        .vertex_buffer(Rc::clone(&vertex_buffer))
        .build(Rc::clone(&rctx))
        .unwrap();
    vertex_array.bind();

    let object_shader_program = ShaderProgramBuilder::new()
        .add_shader(Shader::new(
            rctx.clone(),
            OBJECT_VERTEX_SHADER_SOURCE,
            ShaderType::VertexShader,
        ))
        .add_shader(Shader::new(
            rctx.clone(),
            OBJECT_FRAGMENT_SHADER_SOURCE,
            ShaderType::FragmentShader,
        ))
        .build(rctx.clone());

    let box_image = Image::new("resources/container2.png")?;
    let box_texture = Texture::from_image(Rc::clone(&rctx), box_image).unwrap();
    let box_specular_image = Image::new("resources/container2_specular.png")?;
    let box_specular_texture = Texture::from_image(Rc::clone(&rctx), box_specular_image).unwrap();

    let mut previous_time = glfw.get_time() as f32;
    let mut frame = 0;

    let mut mouse_postition = (0.0, 0.0);

    // projection
    let mut projection = Perspective3::new(
        width as f32 / height as f32,
        60.0_f32.to_radians(),
        0.01,
        100.0,
    );

    // camera
    let mut pitch = 0.0;
    let mut yaw = 0.0;
    let mut camera = Camera::new(&V3::new(0.0, 0.0, 0.0), 3.0, 0.0, pitch, yaw);

    // Lightning
    let lightning_shader_program = ShaderProgramBuilder::new()
        .add_shader(Shader::new(
            rctx.clone(),
            LIGHTNING_VERTEX_SHADER_SOURCE,
            ShaderType::VertexShader,
        ))
        .add_shader(Shader::new(
            rctx.clone(),
            LIGHTNING_FRAGMENT_SHADER_SOURCE,
            ShaderType::FragmentShader,
        ))
        .build(rctx.clone());

    while !window.should_close() {
        frame += 1;
        let time = glfw.get_time() as f32;
        let diff_time = time - previous_time;
        previous_time = time;
        if diff_time > 0.17 {
            error!("Frame {} took too long: {}", frame, diff_time);
        }
        trace!("Time to render frame {}: {:.5} seconds", frame, diff_time);

        glfw.poll_events();
        for (_, event) in glfw::flush_messages(&events) {
            match event {
                glfw::WindowEvent::Key(Key::Escape, _, Action::Press, _) => {
                    window.set_should_close(true)
                }
                glfw::WindowEvent::Key(Key::W, _, Action::Press | Action::Repeat, _) => {
                    camera.update(camera::Movement::Forward, diff_time);
                }
                glfw::WindowEvent::Key(Key::S, _, Action::Press | Action::Repeat, _) => {
                    camera.update(camera::Movement::Downward, diff_time);
                }
                glfw::WindowEvent::Key(Key::A, _, Action::Press | Action::Repeat, _) => {
                    camera.update(camera::Movement::Left, diff_time);
                }
                glfw::WindowEvent::Key(Key::D, _, Action::Press | Action::Repeat, _) => {
                    camera.update(camera::Movement::Right, diff_time);
                }
                glfw::WindowEvent::Size(new_width, new_height) => {
                    width = new_width;
                    height = new_height;
                    projection.set_aspect(width as f32 / height as f32);
                    unsafe {
                        rctx.gl.Viewport(0, 0, width, height);
                    }
                }
                glfw::WindowEvent::CursorPos(x, y) => {
                    let mut x_offset = x - mouse_postition.0;
                    let mut y_offset = -1_f64 * (y - mouse_postition.1); // since we start from the bottom

                    let sensetivity = 0.1_f64;
                    x_offset *= sensetivity;
                    y_offset *= sensetivity;

                    yaw += x_offset as f32;
                    pitch += y_offset as f32;
                    pitch = clamp(pitch, -89.0, 89.0);
                    camera.set_angle(0_f32.to_radians(), pitch.to_radians(), yaw.to_radians());

                    mouse_postition = (x, y);
                }
                _ => {
                    trace!("Unhandled event {:?}", event)
                }
            }
        }

        unsafe {
            rctx.gl.ClearColor(0.2, 0.3, 0.3, 1.0);
            rctx.gl.Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT);
        }

        object_shader_program.uniform(
            "camera_pos",
            UniformValue::Vector3f32(*camera.get_position()),
        );
        let view = camera.get_view_matrix();

        lightning_shader_program.bind();
        vertex_array.bind();
        for (i, light) in lights.iter().enumerate() {
            let position = Translation3::new(light.x, light.y, light.z);
            let model = position.to_homogeneous();
            let model_view_projection = projection.into_inner() * (view * model);
            lightning_shader_program
                .uniform("mvp", UniformValue::Matrix4f32(model_view_projection));

            object_shader_program.uniform(
                &format!("point_lights[{}].pos", i),
                UniformValue::Vector3f32(light.clone().into()),
            );
            object_shader_program.uniform(
                &format!("point_lights[{}].ambient", i),
                UniformValue::Vector3f32(V3::new(0.2, 0.2, 0.2).into()),
            );
            object_shader_program.uniform(
                &format!("point_lights[{}].diffuse", i),
                UniformValue::Vector3f32(V3::new(0.5, 0.5, 0.5).into()),
            );
            object_shader_program.uniform(
                &format!("point_lights[{}].constant", i),
                UniformValue::Float(1.0),
            );
            object_shader_program.uniform(
                &format!("point_lights[{}].linair", i),
                UniformValue::Float(0.14),
            );
            object_shader_program.uniform(
                &format!("point_lights[{}].quadratic", i),
                UniformValue::Float(0.07),
            );

            unsafe {
                rctx.gl
                    .DrawArrays(gl::TRIANGLES, 0, vertex_buffer.len() as _);
            }
        }

        object_shader_program.use_program();
        for cube in &cubes {
            // model
            let postion = Translation3::new(cube.x, cube.y, cube.z);
            let axis = Unit::new_normalize(Vector3::x());
            let rotation = UnitQuaternion::from_axis_angle(&axis, (0_f32).to_radians());
            let model = Isometry3::from_parts(postion, rotation).to_homogeneous();

            box_texture.bind(0);
            object_shader_program.uniform("material.diffuse", UniformValue::Int(0));
            box_specular_texture.bind(1);
            object_shader_program.uniform("material.specular", UniformValue::Int(1));
            object_shader_program.uniform("material.shininess", UniformValue::Float(32.0));
            object_shader_program.uniform("model", UniformValue::Matrix4f32(model));

            let model_view_projection = projection.into_inner() * (view * model);
            object_shader_program.uniform("mvp", UniformValue::Matrix4f32(model_view_projection));

            vertex_array.bind();
            unsafe {
                rctx.gl
                    .DrawArrays(gl::TRIANGLES, 0, vertex_buffer.len() as _);
            }
        }

        window.swap_buffers();
    }

    Ok(())
}
