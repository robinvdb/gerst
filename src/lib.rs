use image::GenericImageView;
use nalgebra::{Scalar, Vector2, Vector3};

pub mod camera;
pub mod renderer;

//todo: we should be able to create a macro v3![x, y?, z?] that converts to a v3
#[repr(C)]
#[derive(Clone, Copy, Debug)]
pub struct V3<T: Scalar> {
    pub x: T,
    pub y: T,
    pub z: T,
}

impl<T> V3<T>
where
    T: Scalar,
{
    pub fn new(x: T, y: T, z: T) -> Self {
        V3 { x, y, z }
    }
}

impl<T> From<V3<T>> for Vector3<T>
where
    T: Scalar,
{
    fn from(other: V3<T>) -> Self {
        Vector3::new(other.x, other.y, other.z)
    }
}

//todo: we should be able to create a macro v2![x, y?] that converts to a v2
#[repr(C)]
#[derive(Clone, Copy, Debug)]
pub struct V2<T>
where
    T: Scalar,
{
    pub x: T,
    pub y: T,
}

impl<T> V2<T>
where
    T: Scalar,
{
    pub fn new(x: T, y: T) -> Self {
        V2 { x, y }
    }
}

impl<T> From<V2<T>> for Vector2<T>
where
    T: Scalar,
{
    fn from(other: V2<T>) -> Self {
        Vector2::new(other.x, other.y)
    }
}

impl<T> From<V2<T>> for Vector3<T>
where
    T: Scalar + Default,
{
    fn from(other: V2<T>) -> Self {
        Vector3::new(other.x, other.y, T::default())
    }
}

#[repr(C)]
#[derive(Clone, Copy, Debug)]
pub struct Vertex {
    pub position: V3<f32>,
    pub tex_coord: V2<f32>,
}

impl Vertex {
    pub fn new(position: V3<f32>, tex_coord: V2<f32>) -> Self {
        Vertex {
            position,
            tex_coord,
        }
    }
}

pub struct Image {
    width: u32,
    height: u32,
    color_type: image::ColorType,
    data: Vec<u8>,
}

impl Image {
    pub fn new(path: &str) -> Result<Self, image::ImageError> {
        let img = image::open(path)?;
        let img = img.flipv();
        let (width, height) = img.dimensions();
        let color_type = img.color();
        let bytes = img.into_bytes();

        Ok(Image {
            width,
            height,
            color_type,
            data: bytes,
        })
    }
}
