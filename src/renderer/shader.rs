use std::collections::HashMap;
use std::ffi::CString;
use std::rc::Rc;

use log::{debug, error, trace};

use super::RenderingContext;

pub enum ShaderType {
    FragmentShader,
    VertexShader,
}

impl ShaderType {
    pub fn gl(&self) -> u32 {
        match self {
            ShaderType::VertexShader => gl::VERTEX_SHADER,
            ShaderType::FragmentShader => gl::FRAGMENT_SHADER,
        }
    }
}

pub struct Shader {
    rctx: Rc<RenderingContext>,
    location: u32,
}

impl Shader {
    pub fn new(rctx: Rc<RenderingContext>, source: &str, shader_type: ShaderType) -> Self {
        trace!("Creating shader with source code: {}", source);
        let c_str = CString::new(source).unwrap();
        let location = unsafe {
            let location = rctx.gl.CreateShader(shader_type.gl());
            rctx.gl
                .ShaderSource(location, 1, &c_str.as_ptr(), std::ptr::null());
            rctx.gl.CompileShader(location);
            location
        };
        debug!("Created shader located at {}", location);
        Shader { rctx, location }
    }

    pub fn location(&self) -> u32 {
        self.location
    }
}

impl Drop for Shader {
    fn drop(&mut self) {
        debug!("Dropping shader located at {}", self.location);
        unsafe { self.rctx.gl.DeleteShader(self.location) }
    }
}

#[derive(Default)]
pub struct ShaderProgramBuilder {
    shaders: Vec<Shader>,
}

impl ShaderProgramBuilder {
    pub fn new() -> Self {
        ShaderProgramBuilder::default()
    }

    pub fn add_shader(mut self, shader: Shader) -> Self {
        self.shaders.push(shader);
        self
    }

    pub fn build(self, rctx: Rc<RenderingContext>) -> ShaderProgram {
        ShaderProgram::new(rctx, self.shaders)
    }
}

pub enum UniformValue {
    Int(i32),
    Float(f32),
    Vector3f32(nalgebra::Vector3<f32>),
    Matrix4f32(nalgebra::Matrix4<f32>),
}

pub struct ShaderProgram {
    rctx: Rc<RenderingContext>,
    location: u32,
    uniform_locations: HashMap<String, i32>,
}

impl ShaderProgram {
    pub fn new(rctx: Rc<RenderingContext>, shaders: Vec<Shader>) -> Self {
        let location = unsafe { rctx.gl.CreateProgram() };
        for shader in &shaders {
            unsafe { rctx.gl.AttachShader(location, shader.location) }
        }
        unsafe {
            rctx.gl.LinkProgram(location);
        }

        debug!("Created shader program located at {}", location);

        // calculate uniforms
        let uniform_count = unsafe {
            let mut count = 0;
            rctx.gl
                .GetProgramiv(location, gl::ACTIVE_UNIFORMS, &mut count);
            count
        };
        debug!(
            "Shader program {} has {} active uniforms",
            location, uniform_count
        );

        // maximum size of an uniform name. This includes the ending nullbyte (i.e. tex is 4 long.)
        let max_name_len = unsafe {
            let mut len = 0;
            rctx.gl
                .GetProgramiv(location, gl::ACTIVE_UNIFORM_MAX_LENGTH, &mut len);
            len
        };
        debug!(
            "Shader program {} uniforms have a max name size of {}",
            location, max_name_len
        );

        let mut buffer: Vec<i8> = Vec::with_capacity(max_name_len as _);
        let mut map = HashMap::with_capacity(uniform_count as _);

        for i in 0..uniform_count {
            let mut len = 0;
            unsafe {
                rctx.gl.GetActiveUniformName(
                    location,
                    i as _,
                    max_name_len,
                    &mut len,
                    buffer.as_mut_ptr(),
                );
                buffer.set_len(len as usize);
            }

            let name = buffer.iter().map(|x| *x as u8).collect();
            let cname = unsafe { CString::from_vec_unchecked(name) };
            let location: i32 = unsafe { rctx.gl.GetUniformLocation(location, cname.as_ptr()) };
            let name = cname.into_string().unwrap();
            debug!("Found uniform {} on location {}", name, location);
            map.insert(name, location);
        }

        ShaderProgram {
            rctx,
            location,
            uniform_locations: map,
        }
    }

    pub fn use_program(&self) {
        unsafe { self.rctx.gl.UseProgram(self.location) }
    }

    pub fn bind(&self) {
        self.use_program()
    }

    pub fn uniform(&self, name: &str, value: UniformValue) {
        if let Some(location) = self.uniform_locations.get(name) {
            match value {
                UniformValue::Int(value) => unsafe {
                    self.rctx
                        .gl
                        .ProgramUniform1i(self.location, *location, value)
                },
                UniformValue::Float(value) => unsafe {
                    self.rctx
                        .gl
                        .ProgramUniform1f(self.location, *location, value)
                },
                UniformValue::Vector3f32(vector) => unsafe {
                    self.rctx.gl.ProgramUniform3f(
                        self.location,
                        *location,
                        vector.x,
                        vector.y,
                        vector.z,
                    );
                },
                UniformValue::Matrix4f32(matrix) => unsafe {
                    self.rctx.gl.ProgramUniformMatrix4fv(
                        self.location,
                        *location,
                        1,
                        gl::FALSE,
                        matrix.as_slice().as_ptr(),
                    );
                },
            }
        } else {
            error!("Could not find uniform named {}", name);
        }
    }
}

impl Drop for ShaderProgram {
    fn drop(&mut self) {
        debug!("Dropping shader program located at {}", self.location);
        unsafe { self.rctx.gl.DeleteProgram(self.location) }
    }
}
