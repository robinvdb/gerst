use std::rc::Rc;

use super::RenderingContext;
use crate::Vertex;
use crate::V3;
use log::debug;

pub struct Buffer<T> {
    rctx: Rc<RenderingContext>,
    location: u32,
    data: Box<[T]>,
}

impl<T> Buffer<T> {
    pub fn new(rctx: Rc<RenderingContext>, data: Box<[T]>) -> Option<Self> {
        let location = unsafe {
            let mut location = 0;
            rctx.gl.CreateBuffers(1, &mut location);
            location
        };

        if location == 0 {
            return None;
        }

        let len = data.len();
        let size_of_one = std::mem::size_of::<Vertex>();

        unsafe {
            rctx.gl.NamedBufferStorage(
                location,
                (len * size_of_one) as _,
                data.as_ptr() as *const _,
                gl::DYNAMIC_STORAGE_BIT,
            );
        }

        debug!("Created buffer located at {}", location);

        Some(Self {
            rctx,
            location,
            data,
        })
    }

    /// Returns the GPU location of the buffer
    pub fn location(&self) -> u32 {
        self.location
    }

    /// Returns the number of elements in the buffer
    pub fn len(&self) -> usize {
        self.data.len()
    }

    /// Returns true if the buffer has no elements.
    pub fn is_empty(&self) -> bool {
        self.data.is_empty()
    }
}

impl<T> Drop for Buffer<T> {
    fn drop(&mut self) {
        debug!("Dropping buffer located at {}", self.location);
        unsafe { self.rctx.gl.DeleteBuffers(1, &self.location) }
    }
}

pub type VertexBuffer = Buffer<Vertex>;
pub type IndexBuffer = Buffer<V3<u32>>;