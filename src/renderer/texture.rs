use std::rc::Rc;

use log::{debug, trace};

use crate::Image;

use super::RenderingContext;

pub struct Texture {
    rctx: Rc<RenderingContext>,
    location: u32,
    _image: Image,
}

impl Texture {
    pub fn from_image(rctx: Rc<RenderingContext>, img: Image) -> Option<Self> {
        let location = unsafe {
            let mut location = 0;
            rctx.gl.CreateTextures(gl::TEXTURE_2D, 1, &mut location);
            location
        };

        if location == 0 {
            return None;
        }

        unsafe {
            rctx.gl
                .TextureParameteri(location, gl::TEXTURE_WRAP_S, gl::CLAMP_TO_EDGE as _);
            rctx.gl
                .TextureParameteri(location, gl::TEXTURE_WRAP_T, gl::CLAMP_TO_EDGE as _);
            rctx.gl
                .TextureParameteri(location, gl::TEXTURE_MIN_FILTER, gl::NEAREST as _);
            rctx.gl
                .TextureParameteri(location, gl::TEXTURE_MAG_FILTER, gl::NEAREST as _);
        }

        let (internal_img_format, img_format) = match img.color_type {
            image::ColorType::Rgb8 => (gl::RGB8, gl::RGB),
            image::ColorType::Rgb16 => (gl::RGB16, gl::RGB),
            image::ColorType::Rgba8 => (gl::RGBA8, gl::RGBA),
            image::ColorType::Rgba16 => (gl::RGBA16, gl::RGB),
            _ => unimplemented!(),
        };

        unsafe {
            rctx.gl.TextureStorage2D(
                location,
                1,
                internal_img_format,
                img.width as _,
                img.height as _,
            );
            rctx.gl.TextureSubImage2D(
                location,
                0,
                0,
                0,
                img.width as _,
                img.height as _,
                img_format,
                gl::UNSIGNED_BYTE,
                img.data.as_ptr() as *const _,
            );
            rctx.gl.GenerateTextureMipmap(location);
        }

        debug!("Created texture located at {}", location);
        Some(Texture {
            rctx,
            location,
            _image: img,
        })
    }

    pub fn bind(&self, index: u32) {
        trace!("Binding texture located at {} on {}", self.location, index);
        unsafe {
            self.rctx.gl.BindTextureUnit(index, self.location);
        }
    }
}

impl Drop for Texture {
    fn drop(&mut self) {
        debug!("Dropping texture located at {}", self.location);
        unsafe {
            self.rctx.gl.DeleteTextures(1, &self.location);
        }
    }
}
