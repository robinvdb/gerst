use std::rc::Rc;

use gl::types::GLenum;
use log::{debug, trace};
use thiserror::Error;

use crate::Vertex;

use super::{
    buffer::{IndexBuffer, VertexBuffer},
    RenderingContext,
};

#[derive(Debug, Clone)]
struct BufferLayoutElement {
    pub gl_type: GLenum,
    pub count: u32,
    pub size: u32,
}

impl BufferLayoutElement {
    pub fn new(gl_type: GLenum, count: u32, size: u32) -> Self {
        BufferLayoutElement {
            gl_type,
            count,
            size,
        }
    }

    pub fn total_size(&self) -> u32 {
        self.size * self.count
    }
}

#[derive(Default, Clone, Debug)]
pub struct BufferLayoutBuilder {
    elements: Vec<BufferLayoutElement>,
}

impl BufferLayoutBuilder {
    fn new() -> Self {
        BufferLayoutBuilder::default()
    }

    fn add_element(mut self, element: BufferLayoutElement) -> Self {
        self.elements.push(element);
        self
    }

    fn build(&self) -> BufferLayout {
        BufferLayout {
            elements: self.elements.clone(),
        }
    }
}

#[derive(Debug)]
pub struct BufferLayout {
    elements: Vec<BufferLayoutElement>,
}

pub struct VertexArray {
    rctx: Rc<RenderingContext>,
    location: u32,
    _vertex_buffer: Rc<VertexBuffer>,
    _index_buffer: Option<Rc<IndexBuffer>>,
}

#[derive(Error, Debug)]
pub enum VertexArrayBuilderError {
    #[error("no vertex buffer is given to the builder")]
    NoVertexBuffer,
}

pub struct VertexArrayBuilder {
    vertex_buffer: Option<Rc<VertexBuffer>>,
    index_buffer: Option<Rc<IndexBuffer>>,
    buffer_layout: BufferLayout,
}

impl VertexArrayBuilder {
    pub fn new() -> Self {
        let buffer_layout = BufferLayoutBuilder::new()
            .add_element(BufferLayoutElement::new(
                gl::FLOAT,
                3,
                std::mem::size_of::<f32>() as _,
            ))
            .add_element(BufferLayoutElement::new(
                gl::FLOAT,
                2,
                std::mem::size_of::<f32>() as _,
            ))
            .build();

        VertexArrayBuilder {
            vertex_buffer: None,
            index_buffer: None,
            buffer_layout,
        }
    }

    pub fn vertex_buffer(mut self, vertex_buffer: Rc<VertexBuffer>) -> Self {
        self.vertex_buffer = Some(vertex_buffer);
        self
    }

    pub fn index_buffer(mut self, index_buffer: Rc<IndexBuffer>) -> Self {
        self.index_buffer = Some(index_buffer);
        self
    }

    pub fn buffer_layout(mut self, layout: BufferLayout) -> Self {
        self.buffer_layout = layout;
        self
    }

    pub fn build(self, rctx: Rc<RenderingContext>) -> Result<VertexArray, VertexArrayBuilderError> {
        if self.vertex_buffer.is_none() {
            return Err(VertexArrayBuilderError::NoVertexBuffer);
        }

        Ok(VertexArray::new(
            rctx,
            self.vertex_buffer.unwrap(),
            self.index_buffer,
            self.buffer_layout,
        ))
    }
}

impl VertexArray {
    pub fn new(
        rctx: Rc<RenderingContext>,
        vertex_buffer: Rc<VertexBuffer>,
        index_buffer: Option<Rc<IndexBuffer>>,
        buffer_layout: BufferLayout,
    ) -> Self {
        let location = unsafe {
            let mut location = 0;
            rctx.gl.CreateVertexArrays(1, &mut location);
            location
        };

        // add vbo and ibo
        unsafe {
            rctx.gl.VertexArrayVertexBuffer(
                location,
                0,
                vertex_buffer.location(),
                0,
                std::mem::size_of::<Vertex>() as _,
            );
            if let Some(index_buffer) = &index_buffer {
                rctx.gl
                    .VertexArrayElementBuffer(location, index_buffer.location())
            }
        }

        // setup layout
        let mut stride = 0;
        buffer_layout
            .elements
            .iter()
            .enumerate()
            .for_each(|(idx, element)| unsafe {
                rctx.gl.EnableVertexArrayAttrib(location, idx as u32);
                rctx.gl.VertexArrayAttribFormat(
                    location,
                    idx as u32,
                    element.count as i32,
                    element.gl_type,
                    gl::FALSE,
                    stride,
                );
                stride += element.total_size();
                rctx.gl.VertexArrayAttribBinding(location, idx as u32, 0);
            });

        // TODO: remove this hack
        // this is the normal used for lightening
        unsafe {
            let gl = &rctx.gl;
            gl.EnableVertexArrayAttrib(location, 2);
            gl.VertexArrayAttribFormat(location, 2, 3, gl::FLOAT, gl::FALSE, 0);
            gl.VertexArrayAttribBinding(location, 2, 0);
        }

        debug!(
            "Created vertex array located at {} with vertex buffer {} and element buffer {}",
            location,
            vertex_buffer.location(),
            index_buffer
                .clone()
                .map(|index_buffer| index_buffer.location())
                .unwrap_or(0)
        );

        VertexArray {
            rctx,
            location,
            _vertex_buffer: vertex_buffer,
            _index_buffer: index_buffer,
        }
    }

    pub fn bind(&self) {
        trace!("Binding vertex array located at  {}", self.location);
        unsafe { self.rctx.gl.BindVertexArray(self.location) }
    }
}

impl Drop for VertexArray {
    fn drop(&mut self) {
        debug!("Dropping vertex array located at {}", self.location);
        unsafe { self.rctx.gl.DeleteVertexArrays(1, &self.location) }
    }
}
