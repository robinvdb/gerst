use gl::Gl;

pub mod buffer;
pub mod shader;
pub mod texture;
pub mod vertex_array;

pub struct RenderingContext {
    /*
    todo: this should not directly be exposed
    */
    pub gl: Gl,
}

impl RenderingContext {
    pub fn new(gl: Gl) -> Self {
        RenderingContext { gl }
    }
}
