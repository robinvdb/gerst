use nalgebra::{Matrix4, Vector3};
use nalgebra_glm as glm;

use super::V3;

#[derive(Debug)]
pub struct Camera {
    /// Current position of the cemara
    position: Vector3<f32>,
    /// Amount of units the camera moves every seconds
    movement_speed: f32,
    /// Euler roll in radians
    roll: f32,
    /// Euler pitch in radians
    pitch: f32,
    /// Euler yaw in radians
    yaw: f32,
    /// Up direction of the worlds, defaults to the y-axis
    world_up: Vector3<f32>,
}

pub enum Movement {
    Forward,
    Downward,
    Left,
    Right,
}

/// TODO: we might want to cache results for front, right, up and get_view_matrix
impl Camera {
    /// Creates a new camera using euler angles
    /// Expects roll, pitch and yaw in radians.
    pub fn new(position: &V3<f32>, movement_speed: f32, roll: f32, pitch: f32, yaw: f32) -> Self {
        Camera {
            position: (*position).into(),
            movement_speed,
            roll,
            pitch,
            yaw,
            world_up: Vector3::y(),
        }
    }

    /// Delta time has to be in seconds
    pub fn update(&mut self, direction: Movement, delta_time: f32) {
        use Movement::*;

        let velocity = self.movement_speed * delta_time;
        self.position += match direction {
            Forward => self.front() * velocity,
            Downward => self.front() * -velocity,
            Left => self.right() * -velocity,
            Right => self.right() * velocity,
        }
    }

    pub fn get_position(&self) -> &Vector3<f32> {
        &self.position
    }

    pub fn set_position(&mut self, position: &V3<f32>) {
        self.position = (*position).into();
    }

    // Expects roll, pitch and yaw in radians
    pub fn set_angle(&mut self, roll: f32, pitch: f32, yaw: f32) {
        self.roll = roll;
        self.pitch = pitch;
        self.yaw = yaw;
    }

    pub fn front(&self) -> Vector3<f32> {
        let yaw = self.yaw;
        let pitch = self.pitch;

        let x = yaw.cos() * pitch.cos();
        let y = pitch.sin();
        let z = yaw.sin() * pitch.cos();

        let direction = glm::vec3(x, y, z);
        glm::normalize(&direction)
    }

    pub fn right(&self) -> Vector3<f32> {
        glm::normalize(&glm::cross(&self.front(), &self.world_up))
    }

    pub fn up(&self) -> Vector3<f32> {
        glm::normalize(&glm::cross(&self.right(), &self.front()))
    }

    pub fn get_view_matrix(&self) -> Matrix4<f32> {
        let position = self.position;
        let direction = position + self.front();
        let up = self.up();

        glm::look_at(&position, &direction, &up)
    }
}
