#version 460 core

layout(location = 0) in vec3 aPos;
layout(location = 1) in vec2 aTexPos;
layout(location = 2) in vec3 aNormal;

uniform mat4 model;
uniform mat4 mvp;

out vec3 Normal;
out vec2 TexPos;
out vec3 FragPos;


void main()
{
    gl_Position = mvp * vec4(aPos, 1.0);

    Normal = aNormal;
    TexPos = aTexPos;
    FragPos = vec3(model * vec4(aPos, 1.0));
}
