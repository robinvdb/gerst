#version 460 core

struct Material {
    sampler2D diffuse;
    sampler2D specular;
    float shininess;
};

struct DirLight {
    vec3 direction;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

struct PointLight {
    vec3 pos;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;

    float constant;
    float linair;
    float quadratic;
};

in vec3 Normal;
in vec2 TexPos;
in vec3 FragPos;

out vec4 FragColor;

uniform vec3 camera_pos;
uniform Material material;
uniform DirLight dir_light;

#define NR_POINT_LIGHTS 4
uniform PointLight point_lights[NR_POINT_LIGHTS];

vec3 CalculateDirLight(DirLight light, vec3 normal, vec3 view_dir)
{
    vec3 light_dir = normalize(-light.direction);

    // ambient shading
    vec3 ambient = light.ambient * texture(material.diffuse, TexPos).rgb;

    // difuse shading
    float diff = max(dot(normal, light_dir), 0.0);
    vec3 diffuse = light.diffuse * diff * texture(material.diffuse, TexPos).rgb;

    // specular shading
    vec3 reflect_dir = reflect(-light_dir, normal);
    float spec = pow(max(dot(view_dir, reflect_dir), 0.0), material.shininess);
    vec3 specular = light.specular * spec * texture(material.specular, TexPos).rgb;

    return ambient + diffuse + specular;
}

vec3 CalcPointLight(PointLight light, vec3 normal, vec3 frag_pos, vec3 view_dir)
{
    vec3 light_dir = normalize(light.pos - frag_pos);
    vec3 norm = normalize(normal);

    // ambient shading
    vec3 ambient = light.ambient * texture(material.diffuse, TexPos).rgb;

    // difuse shading
    float diff = max(dot(norm, light_dir), 0.0);
    vec3 diffuse = light.diffuse * diff * texture(material.diffuse, TexPos).rgb;

    // specular shading
    vec3 reflect_dir = reflect(-light_dir, norm);
    float spec = pow(max(dot(view_dir, reflect_dir), 0.0), material.shininess);
    vec3 specular = light.specular * spec * texture(material.specular, TexPos).rgb;

    // attenuation
    float distance = length(light.pos - frag_pos);
    float attenuation = 1.0 / (light.constant + light.linair * distance
            + light.quadratic * pow(distance, 2));

    return attenuation * (ambient + diffuse + specular);
}

void main()
{
    vec3 norm = normalize(Normal);
    vec3 view_dir = normalize(camera_pos - FragPos);

    vec3 result = CalculateDirLight(dir_light, norm, view_dir);
    for(int i = 0; i < NR_POINT_LIGHTS; i++)
    {
        PointLight point_light = point_lights[i];
        result += CalcPointLight(point_light, norm, FragPos, view_dir);
    }

    FragColor = vec4(result, 1.0);
}

