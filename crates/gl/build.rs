use gl_generator::{Api, Fallbacks, Profile, Registry, StructGenerator};
use std::env;
use std::fs::File;
use std::path::Path;

fn main() {
    let out_dir = env::var("OUT_DIR").unwrap();
    let file_name = "bindings.rs";
    let path = Path::new(&out_dir).join(file_name);
    let mut opengl_file = File::create(path).unwrap();

    Registry::new(Api::Gl, (4, 6), Profile::Core, Fallbacks::All, [])
        .write_bindings(StructGenerator, &mut opengl_file)
        .unwrap();
}
